# Projects

This folder includes materials of all the CMPE 195 projects and thesis supervised under Prof. Saldamli.  
## Meeting schedule

| time         | Aug.25   | Sep.08   | Sep.22   | Oct.07   | Oct.21   |
|:------------:|:--------:|:--------:|:--------:|:--------:|:--------:|
|   5:30-6:00  |          |          |          |          |          |
|   6:00-6:30  |   22S1   |   22S1   |   22S1   |   22S1   |  22S1    |
|   6:30-7:00  |   22S2   |   22S2   |   22S2   |   22S2   |  22S2    |
|   7:00-7:30  |   21F1   |   21F1   |   21F1   |   21F1   |  21F1    |
|   7:30-8:00  |   21F2   |   21F2   |   21F2   |   21F2   |  21F2    |
|   8:00-8:30  |   21F3   |   21F3   |   21F3   |   21F3   |  21F3    |
|   8:30-9:00  |   21F4   |   21F4   |   21F4   |   21F4   |  21F4    |


| time         | Nov.04   | Nov.18   | Dec.02   | Dec.07   |          |
|:------------:|:--------:|:--------:|:--------:|:--------:|:--------:|
|   5:30-6:00  |          |          |          |          |          |
|   6:00-6:30  |   22S1   |   22S1   |   22S1   |   22S1   |          |
|   6:30-7:00  |   22S2   |   22S2   |   22S2   |   22S2   |          |
|   7:00-7:30  |   21F1   |   21F1   |   21F1   |   21F1   |          |
|   7:30-8:00  |   21F2   |   21F2   |   21F2   |   21F2   |          |
|   8:00-8:30  |   21F3   |   21F3   |   21F3   |   21F3   |          |
|   8:30-9:00  |   21F4   |   21F4   |   21F4   |   21F4   |          |


## Projects

### 2022 Spring
1. *E-Commerce Site for Online Shopping*  
    by Chau Doan, Nguyen M. H. Duong, Quang Tang and Thuy Luu.

2. *Car Rental App*  
    by Yiyi Guan, Hongkai Huang and Jiaxi Lai.

### 2021 Fall
1. *Sign Language Robot*  
    by Phuong Ngo, Thinh Nguyen and Julianne To.

2. *SnapShop*  
    by Israel Aldave, Simon Altamirano, Anh Huynh and Danish Khan.

3. *ScheduleAI: Smart Scheduling Assistant*  
    by Anmol Dhoor, Ging Gonzalez, Srivishnu Vedanthi and Xueer Xue.

4. *Creating an e-Commerce Website for Small Business*  
    by Yong Gui Huang, Quang Nguyen, Thao Ton and Hoang Tran.


### 2021 Summer
1. *Employee Management*  
    by Alireza Mahin Parvar, Elizabeth Na, Aman Kaur and Prince Jassal.

2. *Employee Management*  
    by Yongen Chen, Di Li, Yu Li and Zhaoqin Li.

### 2021 Spring
1. *CmpEndium*  
    by Joshua Hnatek, Calvin Ly, Davin Wong and Wilson Zhang.



### 2020 Fall

1. *Keep it Fresh (Kitchen Inventory App).*  
    by Emil Agonoy, Paolo Banzon, Ming Fung Cheung and Deon Labrador.

2. *Virtual Reality Materials Educational Laboratory*  
    by Derrick Lien, Prabjyot Obhi, Tony Saechao and Chuefeng Vang.

3. *eWatch on watchOS*  
    by Alejandro Chavez-Guerrero, Hanna Oh and Visva Suthar.

4. *Detect Cases of COVID-19 Algorithm*
    by Renad Morrar, Saber Bakar, Min Joung Kim and Naveen Reddy


### 2020 Spring

1. *Smart Door using Facial Recognition*  
    by Tiago Benko dos Anjos, Jacob Balster-Gee, Shervin Suresh and Andre Voloshin.

2. *RaspTech for Growing.*    
    by Amarinder Dhillon, Maneek Dhillon, Gaston Garrido, and Ismael Navarro-Fuentes.

3. *AIes (iOS application that helps the visually impaired people to interact with the world).*  
    by Tomas Chavez, Akash Sindhu, Maninder Singh and Jiawei Zhang.

4. *Face Recognition Security Camera.*  
    by Quang Chu, Patrick Cosim and Tu Vu.

### 2019 Fall

1. *Safe Crowdfunding*  
    by Jeremy Tabujara Renanie Tanola and Kyuhak Yuk. 

### 2019 Spring

1. *Blockchain Car ID and Analytics*  
    by David Bit-heydari, Phillip Rognerud, Shawn D. Suarez and Dominic Varakukala 

### 2018 Fall

1. *Magic Mirror*  
	by Hongkai Chang, Bienvinido Onia, Anthony Tran and Rohit Yadav.  

### 2018 Spring

1. *Syllabus to Calendar Events*  
	by Payam Khosravi, Jonathan Stenshoel and Vivian Tran. 



## Important notes

Every group must create a private repo group members + add cmpe195master@gmail.com as an admin.

Please keep the same directory structure, keep things clean and tidy so that we do not need to search things around.  
 
I would like you to stick to the following directory structure. You need to use git and push your files to the respective directories. “pub” folder would include your report. I want you write your report in two column format with templates (word and latex) available at "template" folder.  
 
**Directory**  
> **--year_groupNumber** (alphabetically first group member's lastname)

> > **--doc** (CMPE195 canvas documents, workbooks, presentation, videos and reports)

> > **--pub** (publication, IEEE formatted project report)

> > > **--fig** (your drawing files, svf, visio, AI, etc.)

> > **--src** (your code goes in here)

> > **--ref** (references, papers, webpages)

> > **--misc** (miscellaneous)



