
from picamera.array import PiRGBArray
from picamera import PiCamera
import numpy as np

import cv2

import os
import sys

import pickle
import RPi.GPIO as GPIO
import time
from time import sleep
 
from PIL import Image 

camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 30
camera.rotation = 270
rawCapture = PiRGBArray(camera, size=(640, 480))

faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
    
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("trainer.yml")
    
baseDir = os.path.dirname(os.path.abspath(__file__))
imageDir = os.path.join(baseDir, "images")
    
def search_for_face():
    
    check = os.listdir(imageDir)
    
    if len(check) == 0:
        print("no faces added to DB yet, adding a face to the database...")
        return 2
    
    print("security mode: scanning for face")
    initial = time.time()
    
    with open('labels', 'rb') as f:
        dicti = pickle.load(f)
        f.close()

    font = cv2.FONT_HERSHEY_SIMPLEX
    count = 1;
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        frame = frame.array
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = faceCascade.detectMultiScale(gray, scaleFactor = 1.5, minNeighbors = 5)
        for (x, y, w, h) in faces:
            roiGray = gray[y:y+h, x:x+w]

            id_, conf = recognizer.predict(roiGray)

            for name, value in dicti.items():
                if value == id_:
                    print(name)
                    print(count)
                    count = count + 1

            if conf >= 80:
                cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
                cv2.putText(frame, name + str(conf), (x, y), font, 2, (0, 0 ,255), 2,cv2.LINE_AA)

        cv2.imshow('frame', frame)
        key = cv2.waitKey(1)

        rawCapture.truncate(0)
        
        if count > 8:
            break

    cv2.destroyAllWindows()
    
    end = time.time()
    total = end - initial
    test = open('/home/pi/2020_Chu_Cosim_Vu/src/FR/Tests/FRTestResults.txt', 'a')
    test.write("%5.4f \n" % (total))
    test.close()
    print("Authorized user detected. Safe acccess enabled.")
    
    return 3
    
    
def add_to_db():
    
    name = input("Name for new entry: ")
    dirName = "./images/" + name
    print(dirName)
    if not os.path.exists(dirName):
        os.makedirs(dirName)
        print("Entry created")
    else:
        print("Name already exists")
        return 3
    
    print("Collecting data to learn about your face. Please stare at the camera until process is done...")
    
    count = 1
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        if count > 30:
            break
        frame = frame.array
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = faceCascade.detectMultiScale(gray, scaleFactor = 1.5, minNeighbors = 5)
        for (x, y, w, h) in faces:
            roiGray = gray[y:y+h, x:x+w]
            fileName = dirName + "/" + name + str(count) + ".jpg"
            cv2.imwrite(fileName, roiGray)
            cv2.imshow("face", roiGray)
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
            count += 1

        cv2.imshow('frame', frame)
        key = cv2.waitKey(1)
        rawCapture.truncate(0)

        if key == 27:
            break

    cv2.destroyAllWindows()
    
    print("Face added to the database. Now updating trainer...")
    
    trainer()
    
    print("Trainer updated. This face can now be used to open the safe")
    return 3
    
    
def unlock_safe():
    print("Toggling safe lock...")
    return 3

def main():
    while True:
        print("Welcome to main:")
        selection = input("1: reset security, 2: unlock safe, 3: add another face to database, 4: quit")
        if selection == '1':
            return 0
        elif selection == '2':
            return 1
        elif selection == '3':
            return 2
        elif selection == '4':
            return 4
        else:
            print("Invalid input")
            
            
def trainer():
    currentId = 1
    labelIds = {}
    yLabels = []
    xTrain = []

    for root, dirs, files in os.walk(imageDir):
        print(root, dirs, files)
        for file in files:
            print(file)
            if file.endswith("png") or file.endswith("jpg"):
                path = os.path.join(root, file)
                label = os.path.basename(root)
                print(label)

                if not label in labelIds:
                    labelIds[label] = currentId
                    print(labelIds)
                    currentId += 1

                id_ = labelIds[label]
                pilImage = Image.open(path).convert("L")
                imageArray = np.array(pilImage, "uint8")
                faces = faceCascade.detectMultiScale(imageArray, scaleFactor=1.1, minNeighbors=5)

                for (x, y, w, h) in faces:
                    roi = imageArray[y:y+h, x:x+w]
                    xTrain.append(roi)
                    yLabels.append(id_)

    with open("labels", "wb") as f:
        pickle.dump(labelIds, f)
        f.close()

    recognizer.train(xTrain, np.array(yLabels))
    recognizer.save("trainer.yml")
    print(labelIds)
    