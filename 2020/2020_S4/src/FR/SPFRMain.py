from SPFRStates import search_for_face, unlock_safe, add_to_db, main

select = {
    0: search_for_face,
    1: unlock_safe,
    2: add_to_db,
    3: main
}

nextState = 0;

while True:
    currentState = select.get(nextState)
    
    nextState = currentState()
    
    if nextState == 4:
        break

# python3 SPFRMain.py