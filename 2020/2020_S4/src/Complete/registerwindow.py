# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'registerwindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
import pymysql 
import glob

class Ui_register(object):
    
    def register_user(self, form):
        username = self.username_input1.text()
        password = self.password_input1.text()
        confirm_password = self.confirm_password_input.text()

        conn=pymysql.connect(host="localhost",user='root',password="",db="account_user", autocommit=True)

        cur=conn.cursor()
        query=("insert into signup(username,password,confirm_password) values(%s,%s,%s)")
        data=cur.execute(query,(username,password,confirm_password))
        if(data):
            self.success(form, self.username_input1.text())


    def check_input(self, form):
        msg = QtWidgets.QMessageBox()
        if (self.username_input1.text() and (self.password_input1.text() == self.confirm_password_input.text())):
            self.register_user(form)
            
        else:
            msg.setText('Invalid input')
            msg.exec_()
            
    def success (self, form, user):
        glob.currentuser = user
        glob.rname = user
        glob.aname = user
        glob.go = 'addface'
        glob.check = 1
        form.close()        

    def setupUi(self, register_window):
        glob.prevgo = 'register'
        register_window.setObjectName("register_window")
        register_window.resize(800, 480)
        
        self.label_register = QtWidgets.QLabel(register_window)
        self.label_register.setGeometry(QtCore.QRect(270, 80, 350, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.label_register.setFont(font)
        self.label_register.setObjectName("label_register")
        
        font2 = QtGui.QFont()
        font2.setPointSize(15)
        
        self.username1 = QtWidgets.QLabel(register_window)
        self.username1.setFont(font2)
        self.username1.setGeometry(QtCore.QRect(210, 140,150 , 30))
        self.username1.setObjectName("username1")

        self.password1 = QtWidgets.QLabel(register_window)
        self.password1.setFont(font2)
        self.password1.setGeometry(QtCore.QRect(210, 200, 150, 30))
        self.password1.setObjectName("password1")

        self.confirm_password = QtWidgets.QLabel(register_window)
        self.confirm_password.setFont(font2)
        self.confirm_password.setGeometry(QtCore.QRect(210, 260, 200, 30))
        self.confirm_password.setObjectName("confirm_password")

        self.username_input1 = QtWidgets.QLineEdit(register_window)
        self.username_input1.setGeometry(QtCore.QRect(400, 140, 250, 40))
        self.username_input1.setObjectName("username_input1")

        self.password_input1 = QtWidgets.QLineEdit(register_window)
        self.password_input1.setGeometry(QtCore.QRect(400, 200, 250, 40))
        self.password_input1.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_input1.setObjectName("password_input1")

        self.confirm_password_input = QtWidgets.QLineEdit(register_window)
        self.confirm_password_input.setGeometry(QtCore.QRect(400, 260, 250, 40))
        self.confirm_password_input.setEchoMode(QtWidgets.QLineEdit.Password)
        self.confirm_password_input.setObjectName("confirm_password_input")

        self.register_button1 = QtWidgets.QPushButton(register_window)
        self.register_button1.setGeometry(QtCore.QRect(355, 320, 100, 50))
        self.register_button1.setObjectName("register_button1")

        self.register_button1.clicked.connect(lambda:self.check_input(register_window))


        self.retranslateUi(register_window)
        QtCore.QMetaObject.connectSlotsByName(register_window)

    def retranslateUi(self, register_window):
        _translate = QtCore.QCoreApplication.translate
        register_window.setWindowTitle(_translate("register_window", "Form"))
        self.label_register.setText(_translate("register_window", "Enter a new main user"))
        self.username1.setText(_translate("register_window", "Username"))
        self.password1.setText(_translate("register_window", "Password"))
        self.confirm_password.setText(_translate("register window", "Confirm Password"))
        self.register_button1.setText(_translate("register_window", "Register"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    register_window = QtWidgets.QMainWindow()
    ui = Ui_register()
    ui.setupUi(register_window)
    register_window.showFullScreen()
    sys.exit(app.exec_())
