# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
import glob
import pymysql

class Ui_login(object):
        
    def login_end(self, form):
        username = self.username_input.text()
        glob.currentuser = username
        glob.aname = username
        glob.go = 'ascanner'
        glob.check = 1
        form.close()
        
    def setupUi(self, MainWindow):
        glob.prevgo = 'login'
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 480)
        
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.title = QtWidgets.QLabel(self.centralwidget)
        self.title.setGeometry(QtCore.QRect(355, 100, 350, 50))
        font = QtGui.QFont()
        font.setPointSize(30)
        font.setBold(True)
        font.setWeight(75)
        self.title.setFont(font)
        self.title.setObjectName("title")
        font2 = QtGui.QFont()
        font2.setPointSize(20)
        
        self.username = QtWidgets.QLabel(self.centralwidget)
        self.username.setFont(font2)
        self.username.setGeometry(QtCore.QRect(260, 170,150 , 40))
        self.username.setObjectName("username")

        self.password = QtWidgets.QLabel(self.centralwidget)
        self.password.setGeometry(QtCore.QRect(260, 240, 150, 40))
        self.password.setObjectName("password")
        self.password.setFont(font2)
        
        self.username_input = QtWidgets.QLineEdit(self.centralwidget)
        self.username_input.setGeometry(QtCore.QRect(390, 170, 250, 40))
        self.username_input.setObjectName("username_input")
        self.password_input = QtWidgets.QLineEdit(self.centralwidget)
        self.password_input.setGeometry(QtCore.QRect(390, 240, 250, 40))
        self.password_input.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_input.setObjectName("password_input")
        self.signin_button = QtWidgets.QPushButton(self.centralwidget)
        self.signin_button.setGeometry(QtCore.QRect(355, 300, 100, 50))
        self.signin_button.setObjectName("signin_button")
        self.signin_button.clicked.connect(lambda:self.check_username(MainWindow))

        MainWindow.setCentralWidget(self.centralwidget)

        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 400, 18))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


    def check_username(self, form):
        msg = QtWidgets.QMessageBox()
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        username = self.username_input.text()
       
        connection = pymysql.connect(host="localhost",user='root',password="",db="account_user")
        cursor = connection.cursor()
        query="select * from signup where username=%s "
        data = cursor.execute(query,(username))
        if(len(cursor.fetchall()) > 0):
            self.check_password(form)
        else:
            msg.setText('Please enter valid username or signup')
            msg.exec_()
            
    def check_password(self, form):
        msg = QtWidgets.QMessageBox()
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        password = self.password_input.text()
        connection = pymysql.connect(host="localhost",user='root',password="",db="account_user")
        cursor = connection.cursor()
        query="select * from signup where password=%s "
        data = cursor.execute(query,(password))
        if(len(cursor.fetchall()) > 0):
            self.login_end(form)
        else:
            msg.setText('Wrong password')
            msg.exec_()



    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Face Recognition Security Camera"))
        self.title.setText(_translate("MainWindow", "Login"))
        self.username.setText(_translate("MainWindow", "Username"))
        self.password.setText(_translate("MainWindow", "Password"))
        self.signin_button.setText(_translate("MainWindow", "Sign in"))

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.showMaximized()
    sys.exit(app.exec_())
