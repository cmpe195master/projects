

from PyQt5 import QtCore, QtGui, QtWidgets
import glob

import cv2

import os
import sys

import pickle
import RPi.GPIO as GPIO
import time

faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
    
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("trainer.yml")
    
baseDir = os.path.dirname(os.path.abspath(__file__))
imageDir = os.path.join(baseDir, "images")

with open('labels', 'rb') as f:
    dicti = pickle.load(f)
    f.close()

class Ui_ascanner(object):
    count = 1;
    start = 0;
    current = 0; 

    def check_timeout(self, form):
        self.current = time.time()
        total = self.current - self.start
        if total > 15:
            glob.timeout = 1
            self.scanner_end(form)
        
    def scanner_end(self, form):
        self.timer.stop()
        self.cap.release()
        glob.aname = ''
        glob.rname = ''        
        glob.go = 'ret'
        glob.check = 1
        form.close()
        
    def search_for_face(self, form):
        
        font = cv2.FONT_HERSHEY_SIMPLEX
        ret, f = self.cap.read()
        
        f = cv2.resize(f, None, fx=1.5, fy=1.5, interpolation=cv2.INTER_AREA)
        # hight, width, channel, step
        h, w, c = f.shape
        rot = cv2.getRotationMatrix2D((h/2, w/2), 90, 1.0)
        f = cv2.warpAffine(f, rot, (w, h))
        g = cv2.cvtColor(f, cv2.COLOR_BGR2GRAY)
        
        faces = faceCascade.detectMultiScale(g, scaleFactor = 1.5, minNeighbors = 5)
        
        for (x, y, w, h) in faces:
            rG = g[y:y+h, x:x+w] 
            rid, conf = recognizer.predict(rG)

            for name, val in dicti.items():
                if val == rid and conf <= 60 and glob.aname == name:
                    self.count = self.count + 1

            if conf <= 60:
                cv2.rectangle(f, (x, y), (x+w, y+h), (255, 0, 0), 2)
                cv2.putText(f, name, (x, y), font, 2, (255, 0, 0), 2,cv2.LINE_AA)
                
        if self.count > 5:
            self.scanner_end(form)
        
        h, w, c = f.shape
        s = c * w   
        f = cv2.cvtColor(f, cv2.COLOR_BGR2RGB)
        
        qi = QtGui.QImage(f.data, w, h, s, QtGui.QImage.Format_RGB888)
        self.image.setPixmap(QtGui.QPixmap.fromImage(qi))
        
        self.check_timeout(form)
 
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 480)
        self.verticalLayout = QtWidgets.QVBoxLayout(MainWindow)
        self.verticalLayout.setObjectName("verticalLayout")
        self.image = QtWidgets.QLabel(MainWindow)
        self.image.setObjectName("image")
        self.image.setGeometry(QtCore.QRect(100, 10, 600, 450))
        self.verticalLayout.addWidget(self.image)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.search_for_face(MainWindow))
        self.cap = cv2.VideoCapture(0)
        self.timer.start(1)
        self.start = time.time()

    def retranslateUi(self, MainWindow):
        trans = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(trans("MainWindow", "Scanning Face"))
        self.image.setText(trans("MainWindow", "Scanning...")) 
 
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_ascanner()
    ui.setupUi(MainWindow)
    MainWindow.showMaximized()
    sys.exit(app.exec_())