# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'search.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from fraddfacepage import trainer
import pymysql 
import glob
import os
import shutil

class Ui_database(object):
    
    def messagebox(self, title, message):
        mess = QtWidgets.QMessageBox()
        mess.setWindowTitle(title)
        mess.setText(message)
        mess.setStandardButtons(QtWidgets.QMessageBox.Ok)
        mess.exec_()
        
    def end_database(self, form):
        glob.go = 'main'
        glob.check = 1
        form.close()
    
    def check_user(self, name):
        connection = pymysql.connect(host="localhost",user='root',password="",db="account_user")
        cursor = connection.cursor()
        query="select username from signup where username=%s "
        data = cursor.execute(query,(name))
        if(len(cursor.fetchall()) > 0):
            return 1
        else:
            return 0

    def add_database(self, form):
        name = self.lineEdit.text()
        result = self.check_user(name)
        if result == 0:
            glob.rname = self.lineEdit.text()
            glob.go = 'addface'
            glob.check = 1
            form.close()
        else:
            self.messagebox('ERROR', 'Name is protected account user')
        
    def delete_database(self):
        name = self.lineEdit.text()
        result = self.check_user(name)
        if result == 0:
            dirName = "./images/" + name 
            if os.path.exists(dirName):
                shutil.rmtree(dirName)
                self.messagebox('WAIT', 'Retraining the recognizer')  
                trainer()
                self.messagebox('', 'Entry deleted')
            else:
                self.messagebox('ERROR', 'Entry not found')
        else:
            self.messagebox('ERROR', 'Name is protected account user')
        
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 480)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        
        self.label.setGeometry(QtCore.QRect(350, 138, 60, 25))
        self.label.setObjectName("label")
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(350, 160, 250, 40))
        self.lineEdit.setObjectName("lineEdit")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 18))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(230, 150, 110, 50))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(lambda:self.add_database(MainWindow))
      
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(230, 230, 110, 50))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(self.delete_database)
        
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(230, 310, 110, 50))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(lambda:self.end_database(MainWindow))

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        glob.prevgo = 'database'
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Edit Database"))
        self.pushButton.setText(_translate("MainWindow", "Add"))
        self.pushButton_2.setText(_translate("MainWindow", "Delete"))
        self.pushButton_3.setText(_translate("MainWindow", "Back"))
        self.label.setText(_translate("MainWindow", "Search:"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_database()
    ui.setupUi(MainWindow)
    MainWindow.showFullScreen()
    sys.exit(app.exec_())
