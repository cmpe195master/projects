# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'registerwindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
import pymysql 
import glob

class Ui_changepassword(object):
    
    def messagebox(self, title, message):
        mess = QtWidgets.QMessageBox()
        mess.setWindowTitle(title)
        mess.setText(message)
        mess.setStandardButtons(QtWidgets.QMessageBox.Ok)
        mess.exec_()
    
    def change(self, form):
        username = glob.currentuser
        password = self.password_input1.text()
        confirm_password = self.confirm_password_input.text()

        conn=pymysql.connect(host="localhost",user='root',password="",db="account_user", autocommit=True)
        cur=conn.cursor()
        query=("update signup set password=%s, confirm_password=%s where username =%s")
        data=cur.execute(query,(password,confirm_password, username))
        query="select * from signup where password=%s and username =%s"
        data = cur.execute(query,(password, username))
        if(len(cur.fetchall()) > 0):
            self.messagebox("","Password changed successfully")
            self.success(form)
        else:
            self.messagebox("ERROR","Could not change password")

    def check_input(self, form):
        msg = QtWidgets.QMessageBox()
        if (self.password_input1.text() == self.confirm_password_input.text()):
            self.change(form)
            
        else:
            msg.setText('Invalid input')
            msg.exec_()
            
    def success (self, form):
        glob.go = 'main'
        glob.check = 1
        form.close()         


    def setupUi(self, change_window):
        change_window.setObjectName("change_window")
        change_window.resize(800, 480)
        
        self.label_register = QtWidgets.QLabel(change_window)
        self.label_register.setGeometry(QtCore.QRect(290, 50, 350, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.label_register.setFont(font)
        self.label_register.setObjectName("label_register")
        
        font2 = QtGui.QFont()
        font2.setPointSize(15)

        self.password1 = QtWidgets.QLabel(change_window)
        self.password1.setFont(font2)
        self.password1.setGeometry(QtCore.QRect(210, 130, 150, 30))
        self.password1.setObjectName("password1")

        self.confirm_password = QtWidgets.QLabel(change_window)
        self.confirm_password.setFont(font2)
        self.confirm_password.setGeometry(QtCore.QRect(210, 190, 200, 30))
        self.confirm_password.setObjectName("confirm_password")

        self.password_input1 = QtWidgets.QLineEdit(change_window)
        self.password_input1.setGeometry(QtCore.QRect(400, 130, 250, 40))
        self.password_input1.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_input1.setObjectName("password_input1")

        self.confirm_password_input = QtWidgets.QLineEdit(change_window)
        self.confirm_password_input.setGeometry(QtCore.QRect(400, 190, 250, 40))
        self.confirm_password_input.setEchoMode(QtWidgets.QLineEdit.Password)
        self.confirm_password_input.setObjectName("confirm_password_input")

        self.register_button1 = QtWidgets.QPushButton(change_window)
        self.register_button1.setGeometry(QtCore.QRect(355, 250, 100, 50))
        self.register_button1.setObjectName("register_button1")
        self.register_button1.clicked.connect(lambda:self.check_input(change_window))

        self.back_button1 = QtWidgets.QPushButton(change_window)
        self.back_button1.setGeometry(QtCore.QRect(355, 310, 100, 50))
        self.back_button1.setObjectName("back_button1")
        self.back_button1.clicked.connect(lambda:self.success(change_window))

        self.retranslateUi(change_window)
        QtCore.QMetaObject.connectSlotsByName(change_window)

    def retranslateUi(self, change_window):
        _translate = QtCore.QCoreApplication.translate
        change_window.setWindowTitle(_translate("change_window", "Form"))
        self.label_register.setText(_translate("change_window", "Enter new password"))
        self.password1.setText(_translate("change_window", "Password"))
        self.confirm_password.setText(_translate("change_window", "Confirm Password"))
        self.register_button1.setText(_translate("change_window", "Change"))
        self.back_button1.setText(_translate("change_window", "Back"))

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    change_window = QtWidgets.QWidget()
    ui = Ui_changepassword()
    ui.setupUi(change_window)
    change_window.showFullScreen()
    sys.exit(app.exec_())
