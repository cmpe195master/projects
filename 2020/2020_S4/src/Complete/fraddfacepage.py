

from PyQt5 import QtCore, QtGui, QtWidgets
import glob

import cv2

import os
from PIL import Image
import numpy as np
import pickle

faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
    
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("trainer.yml")
    
baseDir = os.path.dirname(os.path.abspath(__file__))
imageDir = os.path.join(baseDir, "images")

def trainer():
    cId = 1
    lId = {}
    lab = []
    train = []

    for r, d, fs in os.walk(imageDir):
        for f in fs:
            if f.endswith("png") or f.endswith("jpg"):
                path = os.path.join(r, f)
                label = os.path.basename(r)

                if not label in lId:
                    lId[label] = cId
                    cId += 1

                lid = lId[label]
                pIm = Image.open(path).convert("L")
                imArr = np.array(pIm, "uint8")
                faces = faceCascade.detectMultiScale(imArr, scaleFactor=1.1, minNeighbors=5)

                for (x, y, w, h) in faces:
                    roi = imArr[y:y+h, x:x+w]
                    train.append(roi)
                    lab.append(lid)

    with open("labels", "wb") as f:
        pickle.dump(lId, f)
        f.close()

    recognizer.train(train, np.array(lab))
    recognizer.save("trainer.yml")

class Ui_addface(object):
    count = 1;
    def messagebox(self, title, message):
        mess = QtWidgets.QMessageBox()
        mess.setWindowTitle(title)
        mess.setText(message)
        mess.setStandardButtons(QtWidgets.QMessageBox.Ok)
        mess.exec_()
        
    def add_end(self, form):
        self.messagebox('Wait', 'Retraining the recognizer')        
        trainer()
        self.timer.stop()
        self.cap.release()
        self.window = QtWidgets.QMainWindow()
        glob.aname = ''
        glob.rname = ''
        glob.go = 'ret'
        self.messagebox('', 'User successfully added')
        glob.check = 1
        form.close()
       
    def add_face(self,form):
    
        name = glob.rname
        dirName = "./images/" + name 
        
        if not os.path.exists(dirName):
            os.makedirs(dirName)

        font = cv2.FONT_HERSHEY_SIMPLEX
        ret, f = self.cap.read()
        
        f = cv2.resize(f, None, fx=1.5, fy=1.5, interpolation=cv2.INTER_AREA)
        # hight, width, channel, step
        h, w, c = f.shape
        rot = cv2.getRotationMatrix2D((h/2, w/2), 90, 1.0)
        f = cv2.warpAffine(f, rot, (w, h))
        g = cv2.cvtColor(f, cv2.COLOR_BGR2GRAY)
        
        faces = faceCascade.detectMultiScale(g, scaleFactor = 1.5, minNeighbors = 5)
        
        for (x, y, w, h) in faces:
            rG = g[y:y+h, x:x+w] 
            id_, conf = recognizer.predict(rG)
            fName = dirName + "/" + name + str(self.count) + ".jpg"
            cv2.imwrite(fName, rG)
            cv2.rectangle(f, (x, y), (x+w, y+h), (0, 255, 0), 2)
            self.count += 1
                
        if self.count > 20:
            self.add_end(form)
        
        h, w, c = f.shape
        s = c * w   
        f = cv2.cvtColor(f, cv2.COLOR_BGR2RGB)
        
        qi = QtGui.QImage(f.data, w, h, s, QtGui.QImage.Format_RGB888)
        self.image.setPixmap(QtGui.QPixmap.fromImage(qi))
        
    
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 480)
        self.verticalLayout = QtWidgets.QVBoxLayout(MainWindow)
        self.verticalLayout.setObjectName("verticalLayout")
        self.image = QtWidgets.QLabel(MainWindow)
        self.image.setObjectName("image")
        self.image.setGeometry(QtCore.QRect(100, 10, 600, 450))
        self.verticalLayout.addWidget(self.image)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.add_face(MainWindow))
        self.cap = cv2.VideoCapture(0)
        self.timer.start(1)

    def retranslateUi(self, MainWindow):
        trans = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(trans("MainWindow", "Adding Face"))
        self.image.setText(trans("MainWindow", "Scanning for face to add to DB")) 
 
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_add_face()
    ui.setupUi(MainWindow)
    MainWindow.showMaximized()
    sys.exit(app.exec_())

                       

    