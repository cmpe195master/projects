from PyQt5 import QtCore, QtGui, QtWidgets
from fraddfacepage import trainer
import pymysql
import glob
import os
import shutil

class Ui_Deleteaccountpage(object):
    
    def messagebox(self, title, message):
        mess = QtWidgets.QMessageBox()
        mess.setWindowTitle(title)
        mess.setText(message)
        mess.setStandardButtons(QtWidgets.QMessageBox.Ok)
        mess.exec_()
        
    def delete_account(self, form):
        name = glob.currentuser
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle("Message")
        connection = pymysql.connect(host="localhost",user='root',password="",db="account_user")
        cursor = connection.cursor()
        query="delete from signup where username=%s "
        data = cursor.execute(query,(name))
        query="select * from signup where username=%s "
        data = cursor.execute(query,(name))
        connection.commit()
        if(data == 0):
            connection.close()
            dirName = "./images/" + name 
            if os.path.exists(dirName):
                shutil.rmtree(dirName)
                self.messagebox('WAIT', 'Retraining the recognizer')  
                trainer()
                msg.setText('Successfully deleted account')
                msg.exec_()
                self.delete_end_delete(form)  
        else:
            msg.setText('Account could not be deleted')
            msg.exec_()
            self.delete_end_back(form)
           
    def delete_end_delete(self, form):
        connection = pymysql.connect(host="localhost",user='root',password="",db="account_user") 
        cursor = connection.cursor()
        query="select * from signup"
        data = cursor.execute(query)
        if(len(cursor.fetchall()) <= 0):
            glob.go = 'register'
        else:
            glob.go = 'login'
        glob.currentuser = ''
        glob.check = 1
        form.close()        
        
    def delete_end_back(self, form):
        glob.go = 'main'
        glob.check = 1
        form.close()

    def setupUi(self, Deleteaccountpage):
        Deleteaccountpage.setObjectName("Deleteaccountpage")
        Deleteaccountpage.resize(800, 480)
        self.centralwidget = QtWidgets.QWidget(Deleteaccountpage)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(200, 160, 601, 31))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")

        self.Yes = QtWidgets.QPushButton(self.centralwidget)
        self.Yes.setGeometry(QtCore.QRect(250, 200, 150, 50))
        self.Yes.setObjectName("Yes")
        self.Yes.clicked.connect(lambda:self.delete_account(Deleteaccountpage))
        self.No = QtWidgets.QPushButton(self.centralwidget)
        self.No.setGeometry(QtCore.QRect(400, 200, 150, 50))
        self.No.setObjectName("No")
        self.No.clicked.connect(lambda:self.delete_end_back(Deleteaccountpage))
        Deleteaccountpage.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(Deleteaccountpage)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 18))
        self.menubar.setObjectName("menubar")
        Deleteaccountpage.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(Deleteaccountpage)
        self.statusbar.setObjectName("statusbar")
        Deleteaccountpage.setStatusBar(self.statusbar)

        self.retranslateUi(Deleteaccountpage)
        QtCore.QMetaObject.connectSlotsByName(Deleteaccountpage)

    def retranslateUi(self, Deleteaccountpage):
        _translate = QtCore.QCoreApplication.translate
        Deleteaccountpage.setWindowTitle(_translate("Deleteaccountpage", "MainWindow"))
        self.label.setText(_translate("Deleteaccountpage", "Are you sure you want to delete this account ?"))
        self.Yes.setText(_translate("Deleteaccountpage", "YES"))
        self.No.setText(_translate("Deleteaccountpage", "NO"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Deleteaccountpage = QtWidgets.QMainWindow()
    ui = Ui_Deleteaccountpage()
    ui.setupUi(Deleteaccountpage)
    Deleteaccountpage.showFullScreen()
    sys.exit(app.exec_())
