# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainpage.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
import glob

class Ui_mainpage(object):

    def lock_toggle(self):
        if glob.lock == 0:
            glob.lock = 1
        else:
            glob.lock = 0

    def end_main_reset(self, form):
        glob.lock = 0
        glob.go = 'unlock'
        glob.check = 1
        form.close()        
        
    def end_main_newpass(self, form):
        glob.lock = 0
        glob.go = 'newpass'
        glob.check = 1
        form.close()
        
    def end_main_edit(self, form):
        glob.lock = 0
        glob.go = 'database'
        glob.check = 1
        form.close()
        
    def end_main_full_reset(self, form):
        glob.lock = 0
        glob.go = 'reset'
        glob.check = 1
        form.close()
        
    def setupUi(self, Form):
        Form.setObjectName("Main page")
        Form.resize(800, 480)
       
        self.hello = QtWidgets.QLabel(Form)
        self.hello.setGeometry(QtCore.QRect(200, 190, 220, 60))
        font = QtGui.QFont()
        font.setPointSize(50)
        self.hello.setFont(font)
        self.hello.setObjectName("hello")

        self.lock_or_unlock = QtWidgets.QPushButton(Form)
        self.lock_or_unlock.setGeometry(QtCore.QRect(440, 60, 150, 50))
        self.lock_or_unlock.setObjectName("lock_unlock")
        self.lock_or_unlock.clicked.connect(self.lock_toggle)

        self.edit_access = QtWidgets.QPushButton(Form)
        self.edit_access.setGeometry(QtCore.QRect(440, 130, 150, 50))
        self.edit_access.setObjectName("edit_access")
        self.edit_access.clicked.connect(lambda:self.end_main_edit(Form))

        self.reset = QtWidgets.QPushButton(Form)
        self.reset.setGeometry(QtCore.QRect(440, 200, 150, 50))
        self.reset.setObjectName("reset")
        self.reset.clicked.connect(lambda:self.end_main_reset(Form))
        
        self.new_password = QtWidgets.QPushButton(Form)
        self.new_password.setGeometry(QtCore.QRect(440, 270, 150, 50))
        self.new_password.setObjectName("new_password")
        self.new_password.clicked.connect(lambda:self.end_main_newpass(Form))

        self.full_reset = QtWidgets.QPushButton(Form)
        self.full_reset.setGeometry(QtCore.QRect(440, 340, 150, 50))
        self.full_reset.setObjectName("full_reset")
        self.full_reset.clicked.connect(lambda:self.end_main_full_reset(Form))
       
        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
       
        Form.setWindowTitle(_translate("Form", "Main page"))
        self.hello.setText(_translate("Form", "Hello!"))
        
        self.lock_or_unlock.setText(_translate("Form", "Lock/Unlock"))
        self.edit_access.setText(_translate("Form", "Edit Access"))
        self.reset.setText(_translate("Form", "reset"))
        self.new_password.setText(_translate("Form", "New Password"))
        self.full_reset.setText(_translate("Form", "Delete Account"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_mainpage()
    ui.setupUi(Form)
    Form.showFullScreen()
    sys.exit(app.exec_())

