
# go values:
# 0 'ret' = controller determines naxt page
# 1 'lock' = go to lock window
# 2 'unlock' = go to unlock window
# 3 'scanner' = go to scanner window
# 4 'addface' = go to addface window
# 5 'login' = go to login page
# 6 'register' = go to register page
# 7 'database' = go to database page
# 8 'newpass' = go to new password page
# 9  'reset' = delete all database entries and go to register page

def init():
    global lock # control lock to unlock/relock
    lock = 0
    global currentuser
    currentuser =''
    global check # allow controller to change to the next window
    check = 1
    global timeout # timeout occured during scanning
    timeout = 0
    global prevgo
    prevgo = 'unlock'
    global rname # name for face recogntion (add face)
    rname = ''
    global aname # account name
    aname = ''
    global go # log for recording which window to go to
    go = 'unlock' # default go to lock screen
