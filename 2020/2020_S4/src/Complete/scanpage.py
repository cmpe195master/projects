from PyQt5 import QtCore, QtGui, QtWidgets

import cv2

# import SPFRStates

class Ui_ScanFace(object):
    def scanner(self):
        print("performing scan \n")
        result = SPFRStates.search_for_face()      
        
    def setupUi(self, ScanFace):
        ScanFace.setObjectName("ScanFace")
        ScanFace.resize(800, 480)
        self.centralwidget = QtWidgets.QWidget(ScanFace)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(50, 160, 120, 261))
        self.groupBox.setAlignment(QtCore.Qt.AlignJustify|QtCore.Qt.AlignVCenter)
        self.groupBox.setObjectName("groupBox")
        self.Scan = QtWidgets.QPushButton(self.groupBox)
        self.Scan.setGeometry(QtCore.QRect(0, 80, 61, 21))
        self.Scan.setObjectName("Scan")
        self.Scan.clicked.connect(self.scanner)
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(200, 60, 500, 350))
        self.widget.setObjectName("widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.cameraSurface = QtWidgets.QLabel(self.widget)
        self.cameraSurface.setMinimumSize(QtCore.QSize(640, 480))
        self.cameraSurface.setFrameShape(QtWidgets.QFrame.Box)
        self.cameraSurface.setText("")
        self.cameraSurface.setAlignment(QtCore.Qt.AlignCenter)
        self.cameraSurface.setObjectName("cameraSurface")
        self.verticalLayout.addWidget(self.cameraSurface)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(200, 40, 90, 16))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        ScanFace.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(ScanFace)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 18))
        self.menubar.setObjectName("menubar")
        ScanFace.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(ScanFace)
        self.statusbar.setObjectName("statusbar")
        ScanFace.setStatusBar(self.statusbar)

        self.retranslateUi(ScanFace)
        QtCore.QMetaObject.connectSlotsByName(ScanFace)

    def retranslateUi(self, ScanFace):
        _translate = QtCore.QCoreApplication.translate
        ScanFace.setWindowTitle(_translate("ScanFace", "scanpage"))
        self.groupBox.setTitle(_translate("ScanFace", "Control"))
        self.Scan.setText(_translate("ScanFace", "Scan"))
        self.label.setText(_translate("ScanFace", "Camera"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ScanFace = QtWidgets.QMainWindow()
    ui = Ui_ScanFace()
    ui.setupUi(ScanFace)
    ScanFace.showMaximized()
    sys.exit(app.exec_())
