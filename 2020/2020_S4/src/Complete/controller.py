from PyQt5 import QtCore, QtGui, QtWidgets

import glob
# go values:
# 0 'ret' = controller determines naxt page
# 1 'lock' = go to lock window
# 2 'unlock' = go to unlock window
# 3 'scanner' = go to scanner window
# 4 'ascanner' = go to account scanner window
# 5 'addface' = go to addface window
# 6 'login' = go to login page
# 7 'register' = go to register page
# 8 'database' = go to database page
# 9 'newpass' = go to new password page
# 10'main' = go to main page
# 11'reset' = delete all database entries and go to register page
import RPi.GPIO as GPIO
import pymysql

from mainpage import  Ui_mainpage
from unlockpage import Ui_unlock
from lockpage import Ui_lockpage
from frscannerpage import Ui_scanner
from fraccountscannerpage import Ui_ascanner
from registerwindow import Ui_register
from fraddfacepage import Ui_addface
from loginpage import Ui_login
from databasepage import Ui_database
from deleteaccountpage import Ui_Deleteaccountpage
from changepassword import Ui_changepassword

class Ui_controller(object):
    def __init__(self):  
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.control)
        self.timer.start(100)
        
    def check_previous_account(self):
        connection = pymysql.connect(host="localhost",user='root',password="",db="account_user") 
        cursor = connection.cursor()
        query="select * from signup"
        data = cursor.execute(query)
        if(len(cursor.fetchall()) <= 0):
            glob.go = 'register'
     
    def check_lock(self, l):
        if l == 1: 
            GPIO.output(26, GPIO.HIGH)
        else:
            GPIO.output(26, GPIO.LOW)
        
    def main_page(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_mainpage()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def unlock_page(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_unlock()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def lock_page(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_lockpage()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def scanner_page(self):
        GPIO.output(19, GPIO.HIGH)
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_scanner()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def ascanner_page(self):
        GPIO.output(19, GPIO.HIGH)
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_ascanner()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def register_page(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_register()
        self.ui.setupUi(self.window)
        self.window.show() 
 
    def addface_page(self):
        GPIO.output(19, GPIO.HIGH)
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_addface()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def login_page(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_login()
        self.ui.setupUi(self.window)
        self.window.show()
        
    def database_page(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_database()
        self.ui.setupUi(self.window)
        self.window.show()        
 
    def delete_page(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_Deleteaccountpage()
        self.ui.setupUi(self.window)
        self.window.show()

    def change_page(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_changepassword()
        self.ui.setupUi(self.window)
        self.window.showFullScreen()
        
    def control(self):
        self.check_lock(glob.lock)   
        if glob.check > 0:
            GPIO.output(19, GPIO.LOW)
            glob.check = 0
            if glob.go == 'ret':
                if glob.timeout > 0:
                    self.unlock_page()
                    glob.timeout = 0
                elif glob.prevgo == 'unlock':
                    self.lock_page()
                elif glob.prevgo == 'login':
                    self.main_page()
                elif glob.prevgo == 'register':
                    self.main_page()
                elif glob.prevgo == 'database':
                    self.database_page()
                
            elif glob.go == 'lock':
                self.lock_page()
            elif glob.go == 'unlock':
                glob.currentuser = ''
                self.unlock_page()
            elif glob.go == 'scanner':
                self.scanner_page()
            elif glob.go == 'ascanner':
                self.ascanner_page()
            elif glob.go == 'addface':
                self.addface_page()
            elif glob.go == 'login':
                self.login_page()
            elif glob.go == 'register':
                self.register_page()
            elif glob.go == 'database':
                self.database_page()
            elif glob.go == 'newpass':
                self.change_page()
            elif glob.go == 'main':
                self.main_page()
            elif glob.go == 'reset':
                self.delete_page()      

    def setupUi(self, Form):
        self.check_previous_account()
        Form.setObjectName("Face Safe")
        Form.resize(800, 480)
        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
       
        Form.setWindowTitle(_translate("Form", "Face Safe"))



if __name__ == "__main__":
    import sys
    
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(26,GPIO.OUT)
    GPIO.setup(19,GPIO.OUT)
    GPIO.setup(13,GPIO.OUT)
    GPIO.output(13, GPIO.HIGH)
    
    glob.init()
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_controller()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())