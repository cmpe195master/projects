# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'registerwindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
import pymysql 
from successwindow import Ui_success_window

class Ui_register_window(object):
    def messagebox(self, title, message):
        mess = QtWidgets.QMessageBox()
        mess.setWindowTitle(title)
        mess.setText(message)
        mess.setStandardButtons(QtWidgets.QMessageBox.Ok)
        mess.exec_()

    
    def register_user(self):
        username = self.username_input1.text()
        password = self.password_input1.text()
        confirm_password = self.confirm_password_input.text()

        conn=pymysql.connect(host="localhost",user='root',password="",db="account_user", autocommit=True)

        cur=conn.cursor()
        query=("insert into signup(username,password,confirm_password) values(%s,%s,%s)")
        data=cur.execute(query,(username,password,confirm_password))
        if(data):
            self.messagebox("congrat","you are successfully sign up")


        

    def check_input(self):
        msg = QtWidgets.QMessageBox()
        if (self.username_input1.text() and self.password_input1.text()) :
            self.register_button1.clicked.connect(self.success)
            self.register_button1.clicked.connect(self.register_user)
            
        elif (self.username_input1.text() or self.password_input1.text()):
            msg.setText('Invalid input')
            msg.exec_()
        elif not(self.username_input1.text()and self.password_input1.text()):
            msg.setText('Invalid input')
            msg.exec_()
            
    def success (self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_success_window()
        self.ui.setupUi(self.window)
        self.window.showFullScreen()


    def setupUi(self, register_window):
        register_window.setObjectName("register_window")
        register_window.resize(800, 480)
        
        self.label_register = QtWidgets.QLabel(register_window)
        self.label_register.setGeometry(QtCore.QRect(340, 80, 350, 40))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_register.setFont(font)
        self.label_register.setObjectName("label_register")
        self.username1 = QtWidgets.QLabel(register_window)
        self.username1.setGeometry(QtCore.QRect(280, 140,150 , 20))
        self.username1.setObjectName("username1")

        self.password1 = QtWidgets.QLabel(register_window)
        self.password1.setGeometry(QtCore.QRect(280, 200, 150, 20))
        self.password1.setObjectName("password1")

        self.confirm_password = QtWidgets.QLabel(register_window)
        self.confirm_password.setGeometry(QtCore.QRect(280, 260, 150, 20))
        self.confirm_password.setObjectName("confirm_password")



        self.username_input1 = QtWidgets.QLineEdit(register_window)
        self.username_input1.setGeometry(QtCore.QRect(420, 140, 160, 25))
        self.username_input1.setObjectName("username_input1")

        self.password_input1 = QtWidgets.QLineEdit(register_window)
        self.password_input1.setGeometry(QtCore.QRect(420, 200, 160, 25))
        self.password_input1.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_input1.setObjectName("password_input1")

        self.confirm_password_input = QtWidgets.QLineEdit(register_window)
        self.confirm_password_input.setGeometry(QtCore.QRect(420, 260, 160, 25))
        self.confirm_password_input.setEchoMode(QtWidgets.QLineEdit.Password)
        self.confirm_password_input.setObjectName("confirm_password_input")


        
        self.register_button1 = QtWidgets.QPushButton(register_window)
        self.register_button1.setGeometry(QtCore.QRect(340, 300, 100, 25))
        self.register_button1.setObjectName("register_button1")

        #self.register_button1.clicked.connect(self.register_user)

        self.register_button1.clicked.connect(self.check_input)


        self.retranslateUi(register_window)
        QtCore.QMetaObject.connectSlotsByName(register_window)

    def retranslateUi(self, register_window):
        _translate = QtCore.QCoreApplication.translate
        register_window.setWindowTitle(_translate("register_window", "Form"))
        self.label_register.setText(_translate("register_window", "Enter a new main user"))
        self.username1.setText(_translate("register_window", "Username"))
        self.password1.setText(_translate("register_window", "Password"))
        self.confirm_password.setText(_translate("register window", "Confirm Password"))
        self.register_button1.setText(_translate("register_window", "Register"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    register_window = QtWidgets.QWidget()
    ui = Ui_register_window()
    ui.setupUi(register_window)
    register_window.showFullScreen()
    sys.exit(app.exec_())
