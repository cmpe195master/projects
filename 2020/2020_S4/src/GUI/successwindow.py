# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'successwindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets

from signinwindow import Ui_Form

class Ui_success_window(object):


    def clicked(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_Form()
        self.ui.setupUi(self.window)
        self.window.show()

    def setupUi(self, success_window):
        success_window.setObjectName("success_window")
        success_window.resize(400, 150)
        self.ok_button = QtWidgets.QPushButton(success_window)
        self.ok_button.setGeometry(QtCore.QRect(150, 70, 100, 30))
        self.ok_button.setObjectName("ok_button")
        
        self.ok_button.clicked.connect(self.clicked)
        self.label_success = QtWidgets.QLabel(success_window)
        self.label_success.setGeometry(QtCore.QRect(50, 20, 350, 25))
        self.label_success.setObjectName("label_success")

        self.retranslateUi(success_window)
        QtCore.QMetaObject.connectSlotsByName(success_window)

    def retranslateUi(self, success_window):
        _translate = QtCore.QCoreApplication.translate
        success_window.setWindowTitle(_translate("success_window", "Form"))
        self.ok_button.setText(_translate("success_window", "OK"))
        self.label_success.setText(_translate("success_window", "Your account has been created successfully"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    success_window = QtWidgets.QWidget()
    ui = Ui_success_window()
    ui.setupUi(success_window)
    success_window.show()
    sys.exit(app.exec_())
