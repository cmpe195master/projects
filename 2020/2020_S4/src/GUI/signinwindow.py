# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'signinwindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets

#from mainwindow import Ui_MainWindow
class Ui_Form(object):

    def logout1(self):
        quit()

   

    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(800, 480)
       
        self.label_welcome = QtWidgets.QLabel(Form)
        self.label_welcome.setGeometry(QtCore.QRect(360, 160, 200, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label_welcome.setFont(font)
        self.label_welcome.setObjectName("label_welcome")
        self.lineEdit = QtWidgets.QLabel(Form)
        self.lineEdit.setGeometry(QtCore.QRect(360, 230, 500, 100))
        self.lineEdit.setFont(font)
        self.lineEdit.setObjectName("lineEdit")

        self.add_face = QtWidgets.QPushButton(Form)
        self.add_face.setGeometry(QtCore.QRect(550, 100, 100, 30))
        self.add_face.setObjectName("add_face")
        self.delete_face = QtWidgets.QPushButton(Form)
        self.delete_face.setGeometry(QtCore.QRect(550, 160, 100, 30))
        self.delete_face.setObjectName("delete_face")
        self.lock_unlock = QtWidgets.QPushButton(Form)
        self.lock_unlock.setGeometry(QtCore.QRect(550, 220, 100, 30))
        self.lock_unlock.setObjectName("lock_unlock")
        self.logout = QtWidgets.QPushButton(Form)
        self.logout.setGeometry(QtCore.QRect(550, 280, 100, 30))
        self.logout.setObjectName("logout")

        self.logout.clicked.connect(self.logout1)
        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
       
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label_welcome.setText(_translate("Form", "Welcome!"))
        self.add_face.setText(_translate("Form", "Add Face"))
        self.delete_face.setText(_translate("Form", "Delete Face"))
        self.lock_unlock.setText(_translate("Form", "Lock/Unlock"))
        self.logout.setText(_translate("Form", "Logout"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.showFullScreen()
    sys.exit(app.exec_())
