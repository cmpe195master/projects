# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainpage.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from unlockpage import Ui_unlock
from changepassword import Ui_register_window
from databasepage import Ui_database


class Ui_mainpage(object):
    def password_change(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_register_window()
        self.ui.setupUi(self.window)
        self.window.showFullScreen()

    def edit_database(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_database()
        self.ui.setupUi(self.window)
        self.window.showFullScreen()

    def lock_page(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_unlock()
        self.ui.setupUi(self.window)
        self.window.showFullScreen()

 

    def setupUi(self, Form):
        Form.setObjectName("Main page")
        Form.resize(800, 480)
       
        self.hello = QtWidgets.QLabel(Form)
        self.hello.setGeometry(QtCore.QRect(360, 160, 200, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.hello.setFont(font)
        self.hello.setObjectName("hello")

        self.lineEdit = QtWidgets.QLabel(Form)
        self.lineEdit.setGeometry(QtCore.QRect(360, 200, 500, 100))
        self.lineEdit.setFont(font)
        self.lineEdit.setObjectName("lineEdit")


        self.lock_or_unlock = QtWidgets.QPushButton(Form)
        self.lock_or_unlock.setGeometry(QtCore.QRect(440, 120, 130, 30))
        self.lock_or_unlock.setObjectName("lock_unlock")
        self.lock_or_unlock.clicked.connect(self.lock_page)



        self.edit_access = QtWidgets.QPushButton(Form)
        self.edit_access.setGeometry(QtCore.QRect(440, 180, 130, 30))
        self.edit_access.setObjectName("edit_access")
        self.edit_access.clicked.connect(self.edit_database)

        self.reset = QtWidgets.QPushButton(Form)
        self.reset.setGeometry(QtCore.QRect(440, 240, 130, 30))
        self.reset.setObjectName("reset")

        self.new_password = QtWidgets.QPushButton(Form)
        self.new_password.setGeometry(QtCore.QRect(440, 300, 130, 30))
        self.new_password.setObjectName("new_password")
        self.new_password.clicked.connect(self.password_change)

        self.full_reset = QtWidgets.QPushButton(Form)
        self.full_reset.setGeometry(QtCore.QRect(440, 360, 130, 30))
        self.full_reset.setObjectName("full_reset")

       
        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
       
        Form.setWindowTitle(_translate("Form", "Main page"))
        self.hello.setText(_translate("Form", "Hello!"))
        
        self.lock_or_unlock.setText(_translate("Form", "Lock/Unlock"))
        self.edit_access.setText(_translate("Form", "Edit Access"))
        self.reset.setText(_translate("Form", "reset"))
        self.new_password.setText(_translate("Form", "New Password"))
        self.full_reset.setText(_translate("Form", "Full Reset"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_mainpage()
    ui.setupUi(Form)
    Form.showFullScreen()
    sys.exit(app.exec_())

