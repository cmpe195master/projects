# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from registerwindow import Ui_register_window
from signinwindow import Ui_Form
from successwindow import Ui_success_window
from mainpage import Ui_mainpage
import pymysql

class Ui_MainWindow(object):
    def openRegister(self):
       
        self.window = QtWidgets.QMainWindow()
        
  
        self.ui = Ui_register_window()
        self.ui.setupUi(self.window)
        self.window.showFullScreen()
    def opensignin(self):
        username = self.username_input.text()
        self.window = QtWidgets.QMainWindow()
        
        self.ui = Ui_mainpage()
        self.ui.setupUi(self.window)
        self.ui.lineEdit.setText(username)
        self.window.showFullScreen()
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(600, 400)
        
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.title = QtWidgets.QLabel(self.centralwidget)
        self.title.setGeometry(QtCore.QRect(800, 80, 350, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.title.setFont(font)
        self.title.setObjectName("title")
        self.username = QtWidgets.QLabel(self.centralwidget)

        self.username.setGeometry(QtCore.QRect(820, 140,150 , 20))
        
        self.username.setObjectName("username")

        self.password = QtWidgets.QLabel(self.centralwidget)
        self.password.setGeometry(QtCore.QRect(820, 200, 150, 20))
        self.password.setObjectName("password")

        self.username_input = QtWidgets.QLineEdit(self.centralwidget)
        self.username_input.setGeometry(QtCore.QRect(900, 140, 160, 25))
        self.username_input.setObjectName("username_input")
        self.password_input = QtWidgets.QLineEdit(self.centralwidget)
        self.password_input.setGeometry(QtCore.QRect(900, 200, 160, 25))
        self.password_input.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_input.setObjectName("password_input")
        self.signin_button = QtWidgets.QPushButton(self.centralwidget)
        self.signin_button.setGeometry(QtCore.QRect(870, 250, 100, 30))
        self.signin_button.setObjectName("signin_button")
        self.signin_button.clicked.connect(self.check_username)
       # self.signin_button.clicked.connect(self.opensignin)
        self.register_button = QtWidgets.QPushButton(self.centralwidget)
        self.register_button.setGeometry(QtCore.QRect(1000, 250, 100, 30))
        self.register_button.setObjectName("register_button")

        self.register_button.clicked.connect(self.openRegister)
        MainWindow.setCentralWidget(self.centralwidget)

        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 305, 18))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


    def check_username(self):
        msg = QtWidgets.QMessageBox()
        username = self.username_input.text()
       
        connection = pymysql.connect(host="localhost",user='root',password="",db="account_user")
        cursor = connection.cursor()
        query="select * from signup where username=%s "
        data = cursor.execute(query,(username))
        if(len(cursor.fetchall()) > 0):
            

            self.signin_button.clicked.connect(self.check_password)
        else:
            msg.setText('Pleas ennter valid username or signup')
            msg.exec_()
    def check_password(self):
        msg = QtWidgets.QMessageBox()
        password = self.password_input.text()
        connection = pymysql.connect(host="localhost",user='root',password="",db="account_user")
        cursor = connection.cursor()
        query="select * from signup where password=%s "
        data = cursor.execute(query,(password))
        if(len(cursor.fetchall()) > 0):
            

            self.signin_button.clicked.connect(self.opensignin)
        else:
            msg.setText('Wrong password')
            msg.exec_()



    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Face Recognition Security Camera"))
        self.title.setText(_translate("MainWindow", "Face Recognition Security Camera"))
        self.username.setText(_translate("MainWindow", "Username"))
        self.password.setText(_translate("MainWindow", "Password"))
        self.signin_button.setText(_translate("MainWindow", "Sign in"))
        self.register_button.setText(_translate("MainWindow", "Register"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.showMaximized()
    sys.exit(app.exec_())
