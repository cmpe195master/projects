# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'lockpage.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from unlockpage import Ui_unlock

class Ui_lockpage(object):
    def back_unlock(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_unlock()
        self.ui.setupUi(self.window)
        self.window.showMaximized()







    def setupUi(self, lockpage):
        lockpage.setObjectName("lockpage")
        lockpage.resize(800, 480)
        self.centralwidget = QtWidgets.QWidget(lockpage)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton.setFont(font)
        self.pushButton.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.back_unlock)
        self.verticalLayout.addWidget(self.pushButton)
        lockpage.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(lockpage)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 18))
        self.menubar.setObjectName("menubar")
        lockpage.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(lockpage)
        self.statusbar.setObjectName("statusbar")
        lockpage.setStatusBar(self.statusbar)

        self.retranslateUi(lockpage)
        QtCore.QMetaObject.connectSlotsByName(lockpage)

    def retranslateUi(self, lockpage):
        _translate = QtCore.QCoreApplication.translate
        lockpage.setWindowTitle(_translate("lockpage", "lockpage"))
        self.pushButton.setText(_translate("lockpage", "Relock"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    lockpage = QtWidgets.QMainWindow()
    ui = Ui_lockpage()
    ui.setupUi(lockpage)
    lockpage.showMaximized()
    sys.exit(app.exec_())
