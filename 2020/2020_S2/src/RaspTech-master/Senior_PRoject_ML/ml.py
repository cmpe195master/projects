from joblib import load
from sys import argv, stdout
from sklearn.ensemble import RandomForestClassifier
path='senior_proj.joblib'
def make_pred(values):
    clf = load('./Senior_PRoject_ML/senior_proj.joblib')
    tensor= [values]
    return clf.predict(tensor)[0]


if __name__ == "__main__":
    args = argv[1:]
    if len(args) is not 2:
        print("error unsupported amount of args detected")
        print("there should be 2 args: temperature, humidity")
    else:
        for i in args:
            i = float(i)
        args.append(1.0)
        temp = make_pred(args)
        print(temp)
        stdout.flush()
