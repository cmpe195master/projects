import React from "react";
import Table from "react-bootstrap/Table";
import moment from "moment";
function formatDatesAndItem(arr) {
  for (const item of arr) {
    item["timestamp"] = moment(item["timestamp"]).format("llll");
    delete item["_id"];
  }
  return arr;
}
class Tables extends React.Component {
  constructor(props) {
    super(props);
    let temp = this.props.data;
    //console.log(temp);
    temp = formatDatesAndItem(temp);
    let rows = [];
    let ct=0;
    for (const i of temp) {
      let values = Object.values(i);
      let row = values.map(vals => <td key={vals}>{vals}</td>);
      let rows_act = <tr key={ct++}>{row}</tr>;
      rows.push(rows_act)
    }
    let keys = Object.keys(temp[0]);
    let headers = keys.map(value => <th key={value}>{value}</th>);
    this.state = { keys: headers, row: rows};
  }
  render() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>{this.state.keys}</tr>
        </thead>
        <tbody>{this.state.row}</tbody>
      </Table>
    );
  }
}
export default Tables;
//TODO: add pagination
