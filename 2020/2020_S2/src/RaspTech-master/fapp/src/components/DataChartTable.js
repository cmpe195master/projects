import React from "react";
import Charts from "./Charts";
import { DATA_LABEL_FIELDS } from "./CONSTANTS";
import Spinner from "react-bootstrap/Spinner";
import Tables from "./Tables";

function processData(arr) {
  let arr2 = JSON.stringify(arr);
  arr2 = JSON.parse(arr2);
  let keys = Object.keys(arr[0]);
  let new_keys = keys.filter(fields => !DATA_LABEL_FIELDS.includes(fields));
  let labels = keys.filter(
    fields => DATA_LABEL_FIELDS.includes(fields) && !fields.includes("_id")
  );
  let data = {};
  let data2 = {};
  for (const i of labels) {
    data2[i] = arr[0][i];
  }
  for (const j of new_keys) {
    data[j] = [];
  }

  for (const val of arr2) {
    for (const key of DATA_LABEL_FIELDS) {
      delete val[key];
    }
  }
  return [arr2, data2];
}
class DataChartTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      health: this.props.rawData.health,
      rawData: this.props.rawData.data,
      preppedData: false,
      actual_data: {}
    };
    this.componentDidMount.bind(this);
  }
  componentDidMount() {
    let temp = processData(this.state.rawData);
    this.setState({
      preppedData: true,
      actual_data: {
        data: temp[0],
        health: this.state.health,
        titles: temp[1]
      }
    });
  }
  render() {
    if (!this.state.preppedData) {
      return (
        <div>
          <br />
          <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        </div>
      );
    } else {
      return (
        <div>
          <Charts data={this.state.actual_data} />
          <h2 align='center'>Data Analytics have shown that this plant is considered to be {this.state.health}</h2>
          <Tables data={this.state.rawData} />
        </div>
      );
    }
  }
}
export default DataChartTable;
