import React from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import Spinner from "react-bootstrap/Spinner";
import ToggleButtonGroup from "react-bootstrap/ToggleButtonGroup";
import ToggleButton from "react-bootstrap/ToggleButton";
import AppContext from "./AppContext";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
class AdjustDeviceBehavior extends React.Component {
  constructor(props) {
    super();
    this.componentDidMount.bind(this);
    this.onSubmit.bind(this);
    this.onSubmitBehavior.bind(this);
    this.onChangeBehavior.bind(this);
    this.state = {
      devices: [],
      hasCheckedDevices: false,
      hasCheckedBehavior: false,
      loggedIn: false,
      hasChecked: false,
      selectedButton: null,
      buttons: [],
      behavior: {},
      new_behavior: {},
      error: "",
      message: ""
    };
  }
  makeBehavior(behavior) {
    let keys = Object.keys(behavior);
    let final_behavior = keys.map(key => {
      return (
        <Form.Group controlId={`formBasic${key}`} key={key}>
          <Form.Label>{key}</Form.Label>
          <Form.Control
            type="number"
            name={key}
            placeholder={behavior[key]}
            value={this.state.behavior[key]}
            onChange={this.onChangeBehavior.bind(this)}
          />
        </Form.Group>
      );
    });
    return final_behavior;
  }
  onChangeBehavior(event) {
    if (Number(event.target.value) > 0) {
      let temp = this.state.new_behavior;
      temp[event.target.name] = Number(event.target.value);
      this.setState({
        new_behavior: temp
      });
    }
    else{
        let temp = this.state.new_behavior;
        temp[event.target.name] = 20;
        event.target.value = 20;
        this.setState({
          new_behavior: temp
        });
    }
  }
  makeButtons(data) {
    const buttons = data.map(value => {
      return (
        <ToggleButton
          key={value.deviceId}
          type="radio"
          value={value.deviceId}
          variant="outline-info"
        >
          {value.deviceId}
        </ToggleButton>
      );
    });
    return buttons;
  }

  componentDidMount() {
    let log = this.context;
    //console.log(log);
    axios
      .post("http://localhost:4000/devices", {
        headers: { withCredentials: true }
      })
      .then(res => {
        if (res.data.length >= 1) {
          this.setState({
            hasChecked: true,
            loggedIn: true,
            hasCheckedDevices: true,
            devices: res.data,
            selectedButton: 1,
            buttons: this.makeButtons(res.data)
          });
          return;
        } else {
          this.setState({
            hasChecked: true,
            loggedIn: true,
            hasCheckedDevices: true
          });
          return;
        }
      })
      .catch(error => {
        //console.log("no session");
        this.setState({ loggedIn: false, hasChecked: true });
        //console.log(error.response);
        //console.log(this.state.loggedIn);
        return;
      });
  }
  onSubmit(event) {
    axios("http://localhost:4000/devicebehavior", {
      method: "post",
      withCredentials: true,
      data: { deviceId: this.state.selectedButton }
    }).then(res => {
      let temp = this.makeBehavior(res.data);
      this.setState({
        hasCheckedBehavior: true,
        behavior: temp,
        new_behavior: res.data
      });
    });
    event.preventDefault();
  }
  onChange(event) {
    this.setState({ selectedButton: event });
  }
  onSubmitBehavior(event) {
    const newBehavior = {
      behavior: this.state.new_behavior,
      deviceId: this.state.selectedButton
    };

    axios("http://localhost:4000/changebehavior", {
      method: "post",
      withCredentials: true,
      data: newBehavior
    })
      .then(res => {
       this.setState({message: res.data, error: ''})
      })
      .catch(error =>
        this.setState(() => ({
          error: error.response.data,
          message: " "
        }))
      );
    event.preventDefault();
  }
  render() {
    if (
      !this.state.hasChecked ||
      (this.state.hasChecked &&
        this.state.loggedIn &&
        !this.state.hasCheckedDevices)
    ) {
      return (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      );
    } else if (!this.state.loggedIn && this.state.hasChecked) {
      return <Redirect to="/" />;
    } else if (this.state.loggedIn && this.state.hasChecked) {
      if (this.state.hasCheckedDevices && this.state.devices.length > 0) {
        if (!this.state.hasCheckedBehavior) {
          return (
            <Form
              id="form1"
              className="form"
              ref={form => (this.form = form)}
              onSubmit={e => this.onSubmit(e)}
            >
              <h2>Select a device and submit to view editable behavior.</h2>
              <ToggleButtonGroup
                name="radio"
                onChange={this.onChange.bind(this)}
              >
                {this.state.buttons}
              </ToggleButtonGroup>
              <br />
              <br />
              <Button type="submit">Submit</Button>
            </Form>
          );
        } else if (!this.state.behavior && this.state.hasCheckedBehavior)
          return (
            <div>
              <Form
                id="form1"
                className="form"
                ref={form => (this.form = form)}
                onSubmit={e => this.onSubmit(e)}
              >
                <h2>Select a device and submit to view editable behavior.</h2>
                <ToggleButtonGroup
                  name="radio"
                  onChange={this.onChange.bind(this)}
                >
                  {this.state.buttons}
                </ToggleButtonGroup>
                <br />
                <br />
                <Button type="submit">Submit</Button>
                <h2>Whoops sorry no behavior :(</h2>
              </Form>
            </div>
          );
        else {
          return (
            <div>
              <Form
                id="form1"
                className="form"
                ref={form => (this.form = form)}
                onSubmit={e => this.onSubmit(e)}
              >
                <h2>Select a device and submit to view editable behavior.</h2>
                <ToggleButtonGroup
                  name="radio"
                  onChange={this.onChange.bind(this)}
                  defaultValue={this.state.selectedButton}
                >
                  {this.state.buttons}
                </ToggleButtonGroup>
                <br />
                <br />
                <Button type="submit">Submit</Button>
              </Form>
              <h3>Edit Behavior</h3>
              <Form
                id="myForm"
                className="form"
                ref={form => (this.behaviorForm = form)}
                onSubmit={e => this.onSubmitBehavior(e)}
              >
                <Form.Text style={{ color: "red", fontSize: "18px" }}>
                  {this.state.error}
                </Form.Text>
                <Form.Text style={{ color: "green", fontSize: "18px" }}>
                  {this.state.message}
                </Form.Text>

                {this.state.behavior}
                <Button variant="primary" type="submit">
                  Change Behavior
                </Button>
              </Form>
            </div>
          );
        }
      } else {
        return <h2>Whoops sorry no devices :(</h2>;
      }
    }
  }
}
AdjustDeviceBehavior.contextType = AppContext;
export default AdjustDeviceBehavior;
