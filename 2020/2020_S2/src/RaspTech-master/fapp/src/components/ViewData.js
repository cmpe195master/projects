import React from "react";
import axios from "axios";
import DataChartTable from "./DataChartTable"
import { Redirect } from "react-router-dom";
import Spinner from "react-bootstrap/Spinner";
import ToggleButtonGroup from "react-bootstrap/ToggleButtonGroup";
import ToggleButton from "react-bootstrap/ToggleButton";
import AppContext from "./AppContext";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
class ViewData extends React.Component {
  constructor(props) {
    super();
    this.componentDidMount.bind(this);
    this.onSubmit.bind(this);
    this.state = {
      devices: [],
      hasCheckedDevices: false,
      hasCheckedData: false,
      data: [],
      health: null,
      loggedIn: false,
      hasChecked: false,
      selectedButton: null,
      buttons: []
    };
  }
  makeButtons(data) {
    const buttons = data.map(value => {
      return (
        <ToggleButton
          key={value.deviceId}
          type="radio"
          value={value.deviceId}
          variant="outline-info"
        >
          {value.deviceId}
        </ToggleButton>
      );
    });
    return buttons;
  }

  componentDidMount() {
    let log = this.context;
    //console.log(log);
    axios
      .post("http://localhost:4000/devices", {
        headers: { withCredentials: true }
      })
      .then(res => {
        if (res.data.length >= 1) {
          this.setState({
            hasChecked: true,
            loggedIn: true,
            hasCheckedDevices: true,
            devices: res.data,
            selectedButton: 1,
            buttons: this.makeButtons(res.data),
            raw_data:{}
          });
          return;
        } else {
          this.setState({
            hasChecked: true,
            loggedIn: true,
            hasCheckedDevices: true
          });
          return;
        }
      })
      .catch(error => {
        //console.log("no session");
        this.setState({ loggedIn: false, hasChecked: true });
        //console.log(error.response);
        //console.log(this.state.loggedIn);
        return;
      });
  }
  onSubmit(event) {
    axios("http://localhost:4000/data",  {
      method: "post",
      withCredentials: true,
      data: {deviceId: this.state.selectedButton}
    })
    .then(res =>{
      let health= res.data.health.includes('healthy') ? 'healthy': 'unhealthy'
      if(res)
      this.setState({hasCheckedData: true,data: res.data.data, health: health,rawData:{health:health, data:res.data.data}})
    })
    event.preventDefault();
  }
  onChange(event){
    this.setState({selectedButton: event})
  }
  render() {
    if (
      !this.state.hasChecked ||
      (this.state.hasChecked &&  this.state.loggedIn && !this.state.hasCheckedDevices)
    ) {
      return (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      );
    } else if (!this.state.loggedIn && this.state.hasChecked) {
      return <Redirect to="/" />;
    } else if (this.state.loggedIn && this.state.hasChecked) {
      if (this.state.hasCheckedDevices && this.state.devices.length > 0) {
        if(!this.state.hasCheckedData){
          return (
            <Form
              id="form1"
              className="form"
              ref={form => (this.form = form)}
              onSubmit={e => this.onSubmit(e)}
            >
              <h2>Select a device and submit to view data.</h2>
              <ToggleButtonGroup name='radio' onChange={this.onChange.bind(this)}>
              {this.state.buttons}
              </ToggleButtonGroup>
              <br/>
              <br/>
              <Button type="submit">Submit</Button>
            </Form>
          );
        }
        else if(!this.state.data.length>0 && this.state.hasCheckedData)
        return (
          <Form
            id="form1"
            className="form"
            ref={form => (this.form = form)}
            onSubmit={e => this.onSubmit(e)}
          >
            <h2>Select a device and submit to view data.</h2>
            <ToggleButtonGroup name='radio' onChange={this.onChange.bind(this)}>
            {this.state.buttons}
            </ToggleButtonGroup>
            <br/>
            <br/>
            <Button type="submit">Submit</Button>
            <h2>Whoops sorry no data :(</h2>
          </Form>
        );
        else{
          return(
            <div>
            <Form
            id="form1"
            className="form"
            ref={form => (this.form = form)}
            onSubmit={e => this.onSubmit(e)}
          >
            <h2>Select a device and submit to view data.</h2>
            <ToggleButtonGroup name='radio' onChange={this.onChange.bind(this)} defaultValue={this.state.selectedButton}>
            {this.state.buttons}
            </ToggleButtonGroup>
            <br/>
            <br/>
            <Button type="submit">Submit</Button>
          </Form>
          <DataChartTable rawData={this.state.rawData}/>
          </div>
          )
        }
      } else {
        return <h2>Whoops sorry no devices :(</h2>;
      }
    }
  }
}
ViewData.contextType = AppContext;
export default ViewData;
//TODO: add case for selecting device with no data
