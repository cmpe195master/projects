import React from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import AppContext from "./AppContext";
class Logout extends React.Component {
  constructor(props) {
    super();
    this.componentDidMount.bind(this);
    this.state = { loggedOut: false };
  }

  componentDidMount() {
    let log = this.context;
    //console.log(log);
    axios
      .post("http://localhost:4000/logout", {
        headers: { withCredentials: true }
      })
      .then(res => {
        //console.log(res);
        if(log.isLoggedIn){
            log.changeLoginStatus();
        }
        this.setState({ loggedOut: true});
        return;
      })
      .catch(error => {
        //console.log("Logout error");
        this.setState({ loggedOut: false});
        //console.log(error.response);
        //console.log(this.state.loggedIn);
        return;
      });
  }

  render() {
    if (this.state.loggedOut === true) {
      return <Redirect to="/" />;
    } else {
        return (null);
    }
  }
}
Logout.contextType = AppContext;
export default Logout;
