import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import axios from "axios";
import { Redirect } from "react-router-dom";
import AppContext from "./AppContext";
axios.defaults.withCredentials = true;
class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      redirect: false,
      error: false
    };
    this.onSubmit.bind(this);
  }
  onSubmit(event) {
    let log = this.context;
    //make api call and other stuff here, like redirecting
    //Make state the single source of truth as a controlled component
    const returnUser = {
      email: this.state.email,
      password: this.state.password
    };

    axios("http://localhost:4000/login", {
      method: "post",
      withCredentials: true,
      data: returnUser
    })
      .then(res => {
        if(!log.isLoggedIn){
          log.changeLoginStatus();
        }
        this.setState(() => ({
          redirect: true
        }));
        //console.log(res);
      })
      .catch(error =>
        this.setState(() => ({
          error: error.response.data
        }))
      );

    this.setState({
      email: "",
      password: ""
    });
    event.preventDefault();
  }
  onChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  render() {
    if (this.state.redirect === true) {
      return <Redirect to="/About" />;
    }
    return (
      <div>
        <h1> Login</h1>
        <Form
          id="myForm"
          className="form"
          ref={form => (this.loginForm = form)}
          onSubmit={e => this.onSubmit(e)}
        >
          <Form.Text style={{ color: "red", fontSize: "18px" }}>
            {this.state.error}
          </Form.Text>

          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              name="email"
              placeholder="Enter email"
              value={this.state.email}
              onChange={this.onChange.bind(this)}
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              name="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.onChange.bind(this)}
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
          <Form.Text>
            If you are not a registered user, click <a href="/signup">here </a>
          </Form.Text>
        </Form>
      </div>
    );
  }
}
Login.contextType = AppContext;
export default Login;
