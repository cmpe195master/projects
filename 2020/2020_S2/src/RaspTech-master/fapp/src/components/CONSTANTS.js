export const SAMPLE_DATA = {
  plant_name: "cactus",
  timestamp: [1, 2, 3, 4, 5],
  temperature: [50, 10, 30, 40, 40],
  humidity: [10, 11, 12, 13, 14]
};

export const SAMPLE_DATA1 = [
  {
    plant_name: "fishman",
    timestamp: 1,
    temperature: 150,
    humidity: 7
  },
  {
    plant_name: "fishman",
    timestamp: 5,
    temperature: 10,
    humidity: 10
  }
];
export const DATA_LABEL_FIELDS= ["deviceId","plant_type","_id"] //,"timestamp"

export const NAVBAR = ["About", "Signup", "Login"];

export const NAVBAR2 = ["About", "View Data", "Adjust Device Behavior", "Logout"];

export const COLORS = [
  "red",
  "blue",
  "green",
  "gray",
  "yellow",
  "gold",
  "purple"
];
