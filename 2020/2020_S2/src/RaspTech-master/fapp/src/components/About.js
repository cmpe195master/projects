import React from "react";
import axios from "axios";
// import { Redirect } from "react-router-dom";
import Spinner from "react-bootstrap/Spinner";
import AppContext from "./AppContext";
import andy from "./../images/andy.jpg";
import maneek from "./../images/maneek.jpg";
import gaston from "./../images/gaston.png";
import ismael from "./../images/Ismael.JPG";
class About extends React.Component {
  constructor(props) {
    super();
    this.componentDidMount.bind(this);
    this.state = { loggedIn: true, hasChecked: false };
  }

  componentDidMount() {
    let log = this.context;
    axios
      .post("http://localhost:4000/session", {
        headers: { withCredentials: true }
      })
      .then(res => {
        //console.log(res);
        this.setState({ loggedIn: true, hasChecked: true });
        return;
      })
      .catch(error => {
        //console.log("no session");
        this.setState({ loggedIn: false, hasChecked: true });
        //console.log(error.response);
        //console.log(this.state.loggedIn);
        return;
      });
  }

  render() {
    if (this.state.loggedIn === false && this.state.hasChecked === true) {
      return (
        <div>
          <h1>We are Rasptech!</h1>
          <p>Our mission is to optimize the process of indoor farming!</p>
          <p>With our world-class and cost-efficient IoT devices, our clients <br/>will be able to 
            see data analytics and monitor their plants from anywhere in the world. 
          </p>
          <p>Meet the team:</p>
          < img src={andy} width="250" height="275" alt="CodingTheSmartWay.com" />
          <h2>Amarinder Dhillon</h2> 
          <p>Embedded Systems Engineer</p>
          < img src={maneek} width="250" height="275" alt="CodingTheSmartWay.com" />
          <h3>Maneek Dhillon</h3>
          <p>Full-Stack Developer</p>
          < img src={gaston} width="250" height="275" alt="CodingTheSmartWay.com" />
          <h4>Gaston Garrido</h4>
          <p>Data Engineer</p>
          < img src={ismael} width="275" height="225" alt="CodingTheSmartWay.com" />
          <h5>Ismael Navarro-Fuentes</h5>
          <p>Embedded Systems Engineer</p>
        </div>
      );
    } else if (this.state.hasChecked === false) {
      return (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      );
    } else {
      return (
        <div>
          <h1>We are Rasptech!</h1>
          <p>Our mission is to optimize the process of indoor farming!</p>
          <p>With our world-class and cost-efficient IoT devices, our clients <br/>will be able to 
            see data analytics and monitor their plants from anywhere in the world. 
          </p>
          <p>Meet the team:</p>
          < img src={andy} width="250" height="275" alt="CodingTheSmartWay.com" />
          <h2>Amarinder Dhillon</h2> 
          <p>Embedded Systems Engineer</p>
          < img src={maneek} width="250" height="275" alt="CodingTheSmartWay.com" />
          <h3>Maneek Dhillon</h3>
          <p>Full-Stack Developer</p>
          < img src={gaston} width="250" height="275" alt="CodingTheSmartWay.com" />
          <h4>Gaston Garrido</h4>
          <p>Data Engineer</p>
          < img src={ismael} width="275" height="225" alt="CodingTheSmartWay.com" />
          <h5>Ismael Navarro-Fuentes</h5>
          <p>Embedded Systems Engineer</p>
        </div>
      );
    }
  }
}
About.contextType = AppContext;
export default About;
