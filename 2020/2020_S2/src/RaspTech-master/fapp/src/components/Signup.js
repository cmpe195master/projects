import React from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import axios from 'axios';
import { Redirect } from 'react-router-dom';
//TODO: reformat elements in signup page to look better
class Signup extends React.Component {
    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            first_name: "",
            last_name: "",
            phone_number: "",
            redirect: false,
            error: false
        }
        this.onSubmit.bind(this)
    }
    onSubmit(event) {
        //make api call and other stuff here, like redirecting
        //Make state the single source of truth as a controlled component
        //console.log(this.state)
        const newUser = {
            email: this.state.email,
            password: this.state.password,
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            phone_number: this.state.phone_number
        };

        axios.post('http://localhost:4000/signup', newUser)
            .then(() => this.setState(() => ({
                redirect: true
            })))
            .catch((error) => this.setState(() => ({
                error: error.response.data
            })))

        this.setState({
            email: "",
            password: "",
            first_name: "",
            last_name: "",
            phone_number: ""
        })
        event.preventDefault()
    }


    onChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })

    }

    render() {
        if (this.state.redirect === true) {
            return <Redirect to='/' />
        }
        return (
            <div>
                <h1> Signup</h1>
                <Form
                    id='myForm-Signup'
                    className="form"
                    ref={form => this.loginForm = form}
                    onSubmit={(e) => this.onSubmit(e)}>
                    
                    <Form.Text style={{color:"red", fontSize:"18px"}}>
                        {this.state.error}
                    </Form.Text>

                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" name="email" placeholder="Enter email" value={this.state.email}
                            onChange={this.onChange.bind(this)} />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                    </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" name="password" placeholder="Password" value={this.state.password}
                            onChange={this.onChange.bind(this)} />
                    </Form.Group>

                    <Form.Group controlId="formBasicFname">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" name="first_name" placeholder="John" value={this.state.first_name}
                            onChange={this.onChange.bind(this)} />
                    </Form.Group>

                    <Form.Group controlId="formBasicLname">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" name="last_name" placeholder="Smith" value={this.state.last_name}
                            onChange={this.onChange.bind(this)} />
                    </Form.Group>

                    <Form.Group controlId="formBasicCellNumber">
                        <Form.Label>Phone Number (format: xxx-xxx-xxxx)</Form.Label>
                        <Form.Control type="tel" name="phone_number" pattern="^\d{10}$" placeholder="4081234567" value={this.state.phone_number}
                            onChange={this.onChange.bind(this)} />
                        <Form.Text className="text-muted">
                            Omit dashes in submission!
                        </Form.Text>
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Sign Up
                    </Button>
                </Form>
            </div>
        )
    }
}
export default Signup