import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  Label,
  ResponsiveContainer
} from "recharts";
import React from "react";
import moment from 'moment'
import { SAMPLE_DATA, COLORS } from "./CONSTANTS";

function formatDates(arr){
for(const item of arr){
  item['timestamp']= moment(item['timestamp']).format('llll')
}
return arr
}
function makeArray(obj) {
  let arr = [];
  let keys = Object.keys(obj);
  for (let i = 0; i < obj.timestamp.length; i++) {
    let temp = {};
    temp[keys[0]] = obj[keys[0]];
    for (let j = 1; j < keys.length; j++) {
      temp[[keys[j]]] = obj[keys[j]][i];
    }
    arr.push(temp);
  }
  const index = keys.indexOf("plant_name");

  if (index > -1) {
    keys.splice(index, 1);
    const index2 = keys.indexOf("timestamp");
    if (index2 > -1) {
      keys.splice(index2, 1);
    }
  }

  let final = {
    vars: keys,
    plant: obj.plant_name,
    data: arr
  };
  return final;
}
function mapToChart(arr) {
  const navLinks = arr.map(value => (
    <Line
      type="monotone"
      dataKey={value}
      stroke={COLORS[arr.indexOf(value)]}
      key={value}
    />
  ));
  return navLinks;
}
class Charts extends React.Component {
  constructor(props) {
    super(props);
    
    let temp1 = makeArray(SAMPLE_DATA);
    temp1["labels"] = mapToChart(temp1.vars);
    let temp = this.props.data;
    //console.log(temp);
    let val = Object.keys(temp.data[0]).filter(
      word =>
        !Object.keys(temp.titles).includes(word) &&
        word !== "timestamp"
    );
    //console.log(val);
    temp["labels"] = mapToChart(val);
    temp.data =  formatDates(temp.data.reverse())
    
    this.state = temp;
    
    //console.log(this.state)
  }
  render() {
    return (
      <div width={1000} height={1000}>
        <h2 align="center"> {"Device "+this.state.titles.deviceId + "'s data for a "+this.state.titles.plant_type }</h2>
        <ResponsiveContainer width="100%" height={300}>
          <LineChart
            data={this.state.data}
            margin={{ top: 20, right: 5, bottom: 5, left: 5 }}
          >
            {this.state.labels}
            <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
            <XAxis dataKey="timestamp">
              <Label value="Timestamp" offset={-5} position="insideBottom" />
            </XAxis>
            <YAxis>
              <Label value="????" offset={0} position="insideLeft" />
            </YAxis>
            <Tooltip />
            <Legend />
          </LineChart>
        </ResponsiveContainer>
      </div>
    );
  }
}
export default Charts;
