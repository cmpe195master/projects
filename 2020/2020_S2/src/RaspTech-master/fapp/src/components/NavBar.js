import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import logo from "./../images/rasp.png";
import Signup from "./Signup";
import AdjustDeviceBehavior from "./AdjustDeviceBehavior"
import Login from "./Login";
import About from "./About";
import * as CONSTANTS from './CONSTANTS'
import AppContext from "./AppContext";
import ViewData from "./ViewData";
import Logout from "./Logout";
//rewrite navbar
function getLinks(list){
  const navLinks = list.map(value => {
   // if(value!== 'logout'){
      return (
        <li className="navbar-item" key={value}>
          <Link to={"/" + value.replace('/ /g', '')} className="nav-link">{value}</Link>
        </li>
    
      )
    //}
    //else {return ()} ADD LOGIC FOR LOGOUT BUTTON HERE 
  });
  return navLinks;
}
class NavBar extends React.Component {
  constructor() {
    super();
    //let firstLink = <Link to="/" className="navbar-brand">RaspTech</Link>
    
    this.state = {
      isLoggedin: false
      //logo: props.logo
    };
  }

  componentDidMount(){
    let log = this.context
    //console.log(log)
    this.setState({ isLoggedin: log.isLoggedin})
  }
  render() {
    return (
      <Router>
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <img src={logo} width="30" height="30" alt="CodingTheSmartWay.com" />
            <Link to="/about" className="navbar-brand">Rasptech for Growing</Link>
            <div className="collpase navbar-collapse">
              <ul className="navbar-nav mr-auto">
                <AppContext.Consumer>
                  {value=>{
                    if(value.isLoggedIn){
                      return getLinks(CONSTANTS.NAVBAR2)
                    }
                    else{
                      return getLinks(CONSTANTS.NAVBAR)
                    }
                  }}
                </AppContext.Consumer>
              </ul>
            </div>
          </nav>
          <br />
          <Route path="/" exact component={Login} />
          <Route path="/login" exact component={Login} />
          <Route path="/signup" exact component={Signup} />
          <Route path="/about" exact component={About} />
          <Route path="/view Data" exact component={ViewData} />
          <Route path="/logout" exact component={Logout} />
          <Route path="/adjust Device Behavior" exact component={AdjustDeviceBehavior} />
        </div>
      </Router>
    );
  }
}
NavBar.contextType= AppContext;
export default NavBar;
