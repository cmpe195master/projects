import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./components/NavBar";
import AppContext from "./components/AppContext";
import Spinner from "react-bootstrap/Spinner";
import axios from "axios";
//work on iterating later
class App extends Component {
  constructor() {
    super();
    this.state = {
      isLoggedIn: false,
      hasChecked: false
    };
  }
  componentDidMount() {
    axios
      .post("http://localhost:4000/session", {
        headers: { withCredentials: true
        }
      })
      .then(res => {
        this.setState({ isLoggedIn: true,hasChecked: true });
        return;
      })
      .catch(error => {
        this.setState({ isLoggedIn: false ,hasChecked: true });
       console.log(error.response.status)
        return;
      });
  }
  changeLoginStatus = () => {
    if (this.state.isLoggedIn === true) {
      this.setState({ isLoggedIn: false });
    } else {
      this.setState({ isLoggedIn: true });
    }
  };
  render() {
    if (!this.state.hasChecked) {
      return (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      );
    }
    else{
    return (
      <AppContext.Provider
        value={{
          isLoggedIn: this.state.isLoggedIn,
          changeLoginStatus: this.changeLoginStatus
        }}
      >
        <Navbar />
      </AppContext.Provider>
    );
  }
}
}

export default App;
