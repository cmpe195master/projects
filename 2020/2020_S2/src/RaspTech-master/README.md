The system-app folder contains the program that is run on the IoT device.
The rest of the files are for the Rasptech web application.

In order to run the web application perform the following steps:
1. After downloading this repository, open a terminal window and go to the directory
2. Run the command "npm install"
3. Now enter the fapp directory
4. Run the command "npm install" again
5. Now go back to the main directory of the application
6. Run the command "npm run start" to start both the backend server and the frontend