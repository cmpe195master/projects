import Adafruit_DHT as dht
import json
import spidev
import os
import time
import RPi.GPIO as GPIO
from datetime import datetime
from time import sleep

from pymongo import MongoClient

client = MongoClient('mongodb+srv://maneek:maneek123@rasptech-8xdqf.mongodb.net/test?retryWrites=true&w=majority')
db = client.get_database('rasptech')
data = db['data']
devices = db['devices']

spi = spidev.SpiDev()
spi.open(0,0)
low = 240
high = 1023

channel = 21
GPIO.setmode(GPIO.BCM)
GPIO.setup(channel, GPIO.OUT)
GPIO.output(channel, GPIO.LOW)

def readChannel(channel):
    spi.max_speed_hz = 1350000
    val = spi.xfer2([1, (8+channel) << 4, 0])
    data = ((val[1] & 3) << 8) +val[2]
    return data

#Set Data pin
DHT = 14
deviceId = 'test1'
behavior={}
res = devices.find_one({'deviceId': 'test1'})
behavior=res.get('behavior')
threshold = 80
while True:
        moisture_data = readChannel(0)
        percentage = ((moisture_data - low)*100) / (high - low)
        if percentage < 0:
            percentage = 0

        time.sleep(1)
        if percentage > threshold:
            GPIO.output(channel, GPIO.HIGH)
        else:
            GPIO.output(channel, GPIO.LOW)

        threshold = behavior.get('moisture')

        h,t = dht.read_retry(dht.DHT22, DHT) #Read data from sensor
        sleep(60 *1* behavior.get('sendDataFrequency(Minutes)'))
        
        #Format data
        humidity = '{0:0.1f}'.format(h)
        temperature = '{0:0.1f}'.format(t)
        timestamp = str(datetime.now())
        
        plantType = 'succulent'
        moisture = 100 - percentage
        
 
        data_gathered = {'deviceId': deviceId, 'timestamp': timestamp, 'plant_type': plantType, 'temperature': temperature, 'humidity': humidity,\
                'moisture': moisture}
        
        res = devices.find_one({'deviceId': 'test1'})
        print(data_gathered)
        print(behavior)
        data.insert_one(data_gathered)
