# System App

Sensors.py is the python script that continously runs on the Raspberry Pi 4 to gather sensor data, configure the sensors, and post the data to the database. 

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install Adafruit_DHT, spidev, and RPi.GPIO.

```bash
pip install Adafruit_DHT
pip install spidev
pip install RPi.GPIO
```

## Usage

```bash
python sensors.py
```