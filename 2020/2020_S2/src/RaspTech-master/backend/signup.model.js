const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let Signup = new Schema({
    email: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    password: {
        type: String,
        trim: true,
        required: true,
        minlength: 6
    },
    first_name: {
        type: String,
        trim: true,
        required: true
    },
    last_name: {
        type: String,
        trim: true,
        required: true
    }, 
    phone_number: {
        type: String,
        trim: true,
        required: true
    },
    devices: [{deviceId: String}]
});
module.exports = mongoose.model('Signup', Signup);