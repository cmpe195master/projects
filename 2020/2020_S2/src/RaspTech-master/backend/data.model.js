const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let Data = new Schema({
    plant_name: {
        type: String
    },
    timestamp: {
        type: String
    },
    temperature: {
        type: Number
    },
    humidity: {
        type: Number
    }
});
module.exports = mongoose.model('Data', Data);