const cors = require("cors");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const raspRoutes = express.Router();
const PORT = 4000;
const Validator = require("fastest-validator");
const v = new Validator();
const argon2 = require("argon2");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
let Signup = require("./signup.model");
const devurl = "http://localhost:3000"; //where the magic happens
const productionurl = "http://localhost:5000";
const awsurl = "3.92.196.165";
const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectID;
let urls = [devurl, productionurl, awsurl];

app.use(function(req, res, next) {
  const origin = req.get("origin");
  if (urls.includes(origin)) {
    res.setHeader("Access-Control-Allow-Origin", origin);
  }

  res.setHeader("Access-Control-Allow-Headers", "Content-Type,Authorization");
  res.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  next();
});
//const cookieParser= require('cookie-parser')
app.use(bodyParser.json());

const CONNECTION_URL =
  "mongodb+srv://maneek:maneek123@rasptech-8xdqf.mongodb.net/test?retryWrites=true&w=majority";
const DATABASE_NAME = "rasptech";

app.listen(PORT, () => {
  MongoClient.connect(
    CONNECTION_URL,
    { useNewUrlParser: true },
    (error, client) => {
      if (error) {
        throw error;
      }
      database = client.db(DATABASE_NAME);
      collection = database.collection("signup");
      devices = database.collection("devices");
      data = database.collection("data");
      console.log("Connected to `" + DATABASE_NAME + "`!");
    }
  );
});

//SESSIONS
var store = new MongoDBStore({
  uri:
    "mongodb+srv://maneek:maneek123@rasptech-8xdqf.mongodb.net/test?retryWrites=true&w=majority",
  collection: "mySessions"
});
store.on("error", function(error) {
  console.log(error);
});
//app.use(cookieParser())
app.use(
  session({
    secret: "This is a secret",
    cookie: {
      maxAge: 1000 * 60 * 5, // 5 minutes
      sameSite: false,
      secure: false
    },
    store: store,
    // Boilerplate options, see:
    // * https://www.npmjs.com/package/express-session#resave
    // * https://www.npmjs.com/package/express-session#saveuninitialized
    saveUninitialized: false,
    resave: false
  })
);

//VALIDATION
const schemaSign = {
  email: {
    type: "email",
    min: 8,
    max: 255
  },
  password: {
    type: "string"
  },
  first_name: {
    type: "string"
  },
  last_name: {
    type: "string"
  },
  phone_number: {
    type: "string"
  }
};
const check = v.compile(schemaSign);
const schemaLogin = {
  email: {
    type: "email",
    min: 8,
    max: 255
  },
  password: {
    type: "string"
  }
};
const valid = v.compile(schemaLogin);

//SIGNUP
raspRoutes.route("/signup").post(function(req, res) {
  if (check(req.body) === true) {
    let details = req.body;
    try {
      argon2.hash(req.body.password).then(hash => {
        details.password = hash;
        let sign = new Signup(details);
        collection.insertOne(sign, (error, result) => {
          if (error) {
            res.status(400).send("Signup failed");
          }
          res.status(200).json({ sign: "sign added successfully" });
        });
      });
    } catch (err) {
      res.status(400).send("Error signing up");
    }
  } else {
    res
      .status(400)
      .send("Invalid text in input fields. Signup failed. Please try again.");
  }
});

//LOGIN
raspRoutes.route("/login").post(function(req, res) {
  if (valid(req.body) === true) {
    collection.findOne({ email: req.body.email }, function(err, log) {
      if (!log) res.status(404).send("User not found");
      else {
        try {
          argon2.verify(log.password, req.body.password).then(value => {
            if (value) {
              req.session.user = { id: log.email, devices: log.devices };
              req.session.save();
              res.status(200).send("Logged In");
            } else {
              res.status(400).send("Incorrect credentials. Try again");
            }
          });
        } catch (err) {
          res.status(400);
        }
      }
    });
  } else {
    res
      .status(400)
      .send("Invalid text in input fields. Login failed. Please try again.");
  }
});

//Logs user out by destroying session
raspRoutes.route("/logout").post(function(req, res) {
  req.session.destroy();
  res.status(200).send("Logged Out");
});

//Check if session valid session can be checked/deleted from req.session no store check required
raspRoutes.route("/session").post(function(req, res) {
  if (req.session.user !== undefined) {
    res.status(200).send("You good");
  } else {
    req.session.destroy();
    res.status(400).send("No Good");
  }
});

raspRoutes.route("/devices").post(function(req, res) {
  if (req.session.user) {
    res.status(200).send(req.session.user.devices);
  } else {
    res.status(400).send("asdsfg");
  }
});
raspRoutes.route("/data").post(function(req, res) {
  if (req.session.user) {
    data
      .find({ deviceId: req.body.deviceId })
      .sort({ timestamp: -1 })
      .toArray(function(err, log) {
        if (!log) res.status(404).send("No Data");
        else {
          let path = "./Senior_PRoject_ML/ml.py";
          let py_path = process.env.CONDA_PYTHON_EXE;
          var childProcess = require("child_process").spawn(py_path, [
            path,
            50,
            20
          ]);
          childProcess.stdout.on("data", function(data) {
            let plant_health = data.toString();
            res.status(200).send({
              health: plant_health,
              data: log
            });
          });
          childProcess.stderr.on("data", function(data) {
            console.log(py_path);
            console.log(data.toString());
            });
        }
      });
  } else {
    res.status(400).send("bad");
  }
});
raspRoutes.route("/devicebehavior").post(function(req, res) {
  if (req.session.user) {
    devices.findOne({ deviceId: req.body.deviceId }, function(err, log) {
      if (!log) res.status(404).send("Invalid Device Id");
      else {
        res.status(200).send(log.behavior);
      }
    });
  } else {
    res.status(400).send("bad");
  }
});
raspRoutes.route("/changebehavior").post(function(req, res) {
  if (req.session.user) {
    let temp= Object.keys(req.body.behavior)
    if (temp.length === 0) {
      res.status(400).send("Cannot change behavior");
    }
    else if(temp.some(val => val<1)){
        res.status(400).send("Invalid update, parameters too small.");
    }
    else{
        devices.findOneAndUpdate(
            { deviceId: req.body.deviceId },
            { $set: { behavior: req.body.behavior } },
            function(err, log) {
              if (!log) res.status(404).send("Invalid Device Id");
              else {
                res
                  .status(200)
                  .send(`Updated ${req.body.deviceId} successfully!`);
              }
            }
          );
    }
    
  } 
});

app.use("/", raspRoutes);
